Finance
=======

Finance

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style

:License: MIT

Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Type checks
^^^^^^^^^^^

Running type checks with mypy:

::

  $ mypy finance

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ pytest

Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Moved to `Live reloading and SASS compilation`_.

.. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html

Deployment
----------

The following details how to deploy this application.

* Create a new database using a postgresql server instance
* Modify config/settings/base.py line 45 and setup your database url
* Create virtual environment::

    $ python3.9 -m venv .venv

* Activate virtual environment::

    $ source <path to your recently created virtual environment>

* Install requirements::

    $ pip install -r requirements/local.txt

* To create a **normal user account**, just go to Sign Up and fill out the form.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

* Running app::

    $ python manage.py runserver 0.0.0.0:8000

* Go and visit your web browser: http://localhost:8000

