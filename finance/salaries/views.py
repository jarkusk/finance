# Create your views here.

from dependency_injector.wiring import inject, Provide
from django.views.generic import TemplateView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from finance.salaries import EmployeesContainer
from finance.salaries import ValidationsContainer
from finance.salaries.services import EmployeesService, ValidationService


class SalariesView(TemplateView):
    permission_classes = (IsAuthenticated,)
    template_name = "salaries/salaries.html"

    @inject
    def get(self, request, service: EmployeesService = Provide[EmployeesContainer.search_service], *args, **kwargs):
        search = request.GET.get('employee_show', None)
        context = self.get_context_data(**kwargs)
        if search and search == 'yes':
            employee_id = request.GET.get('employee', None)
            context['salaries'] = service.get_employees_info(employee_id=employee_id)
        return self.render_to_response(context=context)


salaries_views_list = SalariesView.as_view()


class ValidateAPIView(APIView):

    @inject
    def post(self, request, service: ValidationService = Provide[ValidationsContainer.validation_service], *args, **kwargs):
        try:
            print("Starting")
            service.validate_data(request=request)
            if service.to_return:
                message = f"Validation errors: {service.to_return}"
                return Response(data={"message": message}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            message = f"An unexpected error occurred caused by: {e.__str__()}"
            return Response(data={"message": message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


validate_request = ValidateAPIView.as_view()
