from unittest import mock

from django.test import TestCase
from django.urls import reverse

from finance.salaries import container
from finance.salaries.data_access import EmployeesDAO
from finance.salaries.services import EmployeesService


class IndexTests(TestCase):

    def test_calculate_salary(self):
        dao_mock = mock.Mock(spec=EmployeesDAO)
        dao_mock.list_employees_from_web_api.return_value = [
            {
                "id": 1,
                "name": "Andrea",
                "contractTypeName": "HourlySalaryEmployee",
                "roleId": 1,
                "roleName": "Administrator",
                "roleDescription": None,
                "hourlySalary": 1.0,
                "monthlySalary": 5.0
            }
        ]
        service_mock = mock.Mock(spec=EmployeesService(dao=dao_mock))
        with container.search_service.override(service_mock):
            response = self.client.get(reverse("salaries:salaries_list"))
            print(response)
            self.assertContains(response, 'salaries')
