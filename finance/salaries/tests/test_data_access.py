from unittest import mock

from finance.salaries.data_access import EmployeesDAO

employees_dao = EmployeesDAO()


def test_get_data_ok():
    employees_dao_mock = mock.Mock(EmployeesDAO)
    employees_dao_mock.list_employees_from_web_api.return_value = [
        {
            "id": 1,
            "name": "Andrea",
            "contractTypeName": "HourlySalaryEmployee",
            "roleId": 1,
            "roleName": "Administrator",
            "roleDescription": None,
            "hourlySalary": 10000.0,
            "monthlySalary": 50000.0
        },
        {
            "id": 2,
            "name": "Alex",
            "contractTypeName": "MonthlySalaryEmployee",
            "roleId": 2,
            "roleName": "Contractor",
            "roleDescription": None,
            "hourlySalary": 10000.0,
            "monthlySalary": 50000.0
        }
    ]
    assert len(employees_dao_mock.list_employees_from_web_api()) > 0
    assert employees_dao_mock.list_employees_from_web_api()[0]['monthlySalary'] == 50000.0


def test_get_data_err():
    employees_dao.EMPLOYEES_URL = None
    assert employees_dao.list_employees_from_web_api() == list()
