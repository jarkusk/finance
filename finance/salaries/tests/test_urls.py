import pytest
from django.urls import resolve, reverse

pytestmark = pytest.mark.django_db


def test_salaries_list():
    assert reverse("salaries:salaries_list") == "/salaries/employees/"
    assert resolve("/salaries/employees/").view_name == "salaries:salaries_list"
