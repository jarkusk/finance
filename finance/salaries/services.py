import logging
import re
from datetime import datetime

from django.utils import timezone

from finance.salaries.data_access import EmployeesDAO


class EmployeesService:
    def __init__(self, dao: EmployeesDAO):
        self.employees_dao = dao

    def get_employees_info(self, employee_id: int):
        try:
            # get the data from web api
            data_from_web_api = self.employees_dao.list_employees_from_web_api()
            temp_list = list()
            flag = False
            if data_from_web_api:
                if employee_id and any(d['id'] == int(employee_id) for d in data_from_web_api):
                    flag = True

                for employee in data_from_web_api:
                    if 'contractTypeName' in employee and 'hourlySalary' in employee:
                        if flag is False:
                            self.calculate_annual_salary(employee=employee, temp_list=temp_list)
                        elif flag and employee['id'] == int(employee_id):
                            self.calculate_annual_salary(employee=employee, temp_list=temp_list)
            return temp_list
        except Exception as e:
            logging.error(e.__cause__)
            return list()

    def calculate_annual_salary(self, employee: dict, temp_list: list):
        annual_salary = 120 * int(employee['hourlySalary']) * 12 if employee['contractTypeName'] == 'HourlySalaryEmployee' else 120 * int(employee['monthlySalary'])
        employee['annual_salary'] = annual_salary
        temp_list.append(employee)


class ValidationService:

    def __init__(self):
        self.to_return = dict()

    def validate_data(self, request: dict) -> bool:
        try:
            if request and request.method == 'POST':
                self.validate_id(request=request)
                self.validate_week_number(request=request)
                self.validate_date(request=request)
            else:
                raise Exception("No request value")
        except Exception as e:
            raise e

    def validate_id(self, request: dict) -> None:
        regex = "[A-Za-z0-9]+{8,20}"
        compiled = re.compile(regex)
        _id = request.POST.get("id", None)
        if not _id:
            self.to_return["id"] = "Object is empty or null"
        elif _id and not (compiled.match(_id)):
            self.to_return["id"] = " Object doesn't match with char len validation"

    def validate_week_number(self, request) -> None:
        week_number = request.POST.get("numero_semana", None)
        if not week_number:
            self.to_return["numero_semana"] = "Object is empty or null"
        elif week_number and not self.is_valid_week_number(week_number=week_number):
            self.to_return["week_number"] = "Object is empty or null"

    def validate_date(self, request) -> None:
        now = timezone.now().date
        date = request.POST.get("fecha", None)
        if date.date() > now:
            self.to_return['date'] = "Date is in the future"

    def is_valid_week_number(self, week_number):
        init_date = datetime.date(2021, 1, 1)
        end_date = datetime.date(2021, 12, 31)
        week_number_init = init_date.isocalendar()[1]
        week_number_end = end_date.isocalendar()[1]
        if week_number and not (week_number_end >= week_number >= week_number_init):
            self.to_return["week_number"] = "Invalid week number"
