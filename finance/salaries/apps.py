from django.apps import AppConfig

from finance.salaries import container, views


class SalariesConfig(AppConfig):
    name = 'finance.salaries'

    def ready(self):
        container.wire(modules=[views])
