from django.urls import path

from finance.salaries.views import (
    salaries_views_list,
)

app_name = "salaries"
urlpatterns = [
    path("employees/", view=salaries_views_list, name="salaries_list"),
    path("validate/", view=validate_request, name="validate_request")
]
