from dependency_injector import containers, providers

from .data_access import EmployeesDAO
from .services import EmployeesService


class EmployeesContainer(containers.DeclarativeContainer):
    config = providers.Configuration()

    dao = providers.Factory(
        EmployeesDAO
    )

    search_service = providers.Factory(
        EmployeesService,
        dao=dao,
    )


class ValidationsContainer(containers.DeclarativeContainer):
    config = providers.Configuration()
    validation_service = providers.Factory(
        ValidationService,
    )
