import logging

import requests
from requests import HTTPError
from requests.exceptions import MissingSchema


class EmployeesDAO:
    def __init__(self):
        # this can be placed in a dot env file
        self.EMPLOYEES_URL = 'http://masglobaltestapi.azurewebsites.net/api/Employees'

    def list_employees_from_web_api(self):
        try:
            response = requests.get(self.EMPLOYEES_URL)
            if response.status_code == 200:
                logging.info('Success!')
                return response.json()
            else:
                response.raise_for_status()
        except MissingSchema as misch:
            logging.error(f'No url service provided: {misch.__cause__}')
            return list()
        except HTTPError as http_err:
            logging.error(f'HTTP error occurred: {http_err.__cause__}')
            return list()
        except Exception as e:
            logging.exception(e)
            return list()
